<h1>Java QuickSort</h1>

<p>QuickSort implementation in java which allows sorting List (<code>List&lt;T></code>) and arrays (<code>T[]</code>) of object in O(n log n) time. It also supports 4 types of pivot picking methods - FIRST_ELEMENT, LAST_ELEMENT, MEDIAN or MEDIAN_OF_THREE. For best performance MEDIAN or MEDIAN_OF_THREE is recommended.</p>
<p>More on pivot picking methods: <a href="https://en.wikipedia.org/wiki/Quicksort#Choice_of_pivot">https://en.wikipedia.org/wiki/Quicksort#Choice_of_pivot</a></p>
<br />

<h2>Examples</h2>

<p>Dummy monkey class for better understanding of examples below.</p>
<pre>
public class Monkey {
    public String name;
    public int bananaCapacity;
}
</pre>

<p>Sorting List of <code>Monkey</code> objects. Compare them by <code>bananaCapacity</code> and choose median as pivot.</p>
<pre>
ArrayList<Monkey> monkeys = //unordered ArrayList of Monkeys

QuickSort.sortList(monkeys, new QuickSort.Comparator<Monkey>() {
    @Override
    public int compare(Monkey a, Monkey b) {
        if(a.bananaCapacity == b.bananaCapacity) return 0;
        else if (a.bananaCapacity < b.bananaCapacity) return -1;
        else return 1;
    }
});
</pre>

<p>Sorting List of <code>Monkey</code> objects. Compare them by <code>bananaCapacity</code> and choose median of three as pivot.</p>
<pre>
ArrayList<Monkey> monkeys = //unordered ArrayList of monkeys

QuickSort.sortList(monkeys, QuickSort.PivotMethod.MEDIAN_OF_THREE, new QuickSort.Comparator<Monkey>() {
    @Override
    public int compare(Monkey a, Monkey b) {
        if(a.bananaCapacity == b.bananaCapacity) return 0;
        else if (a.bananaCapacity < b.bananaCapacity) return -1;
        else return 1;
    }
});
</pre>

<p>Sorting array of <code>Monkey</code> objects. Compare them by <code>bananaCapacity</code> and choose median of three as pivot.</p>
<pre>
Monkey[] monkeys = //unordered array of Monkeys

QuickSort.sortArray(monkeys, QuickSort.PivotMethod.MEDIAN_OF_THREE, new QuickSort.Comparator<Monkey>() {
    @Override
    public int compare(Monkey a, Monkey b) {
        if(a.bananaCapacity == b.bananaCapacity) return 0;
        else if (a.bananaCapacity < b.bananaCapacity) return -1;
        else return 1;
    }
});
</pre>

<br />
<h2>Monkey benchmarks</h2>
<p>I have done some benchmarks of sorting different sizes and arangements of <code>ArrayList</code>s of <code>Monkey</code> objects. Benchmarks have been done on Intel i7-6700hq. All test cases have been created in advance and are the same for the same <code>ArrayList</code> size. Time unit is second.</p>

<br />
<p><b>Sorting random <code>ArrayList</code> of <code>Monkey</code> objects.</b></p>
<table>
<tr>
    <th>no. of elements</th>
    <th>qs FIRST_ELEMENT</th>
    <th>qs LAST_ELEMENT</th>
    <th>qs MEDIAN</th>
    <th>qs MEDIAN_OF_THREE</th>
    <th>Collections.sort()</th>
    <th>ArrayList.sort()</th>
</tr><tr>
  <td align="right">10</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.001000</td>
  <td>0.000000</td>
</tr>
<tr>
  <td align="right">100</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
</tr>
<tr>
  <td align="right">1000</td>
  <td>0.000667</td>
  <td>0.000000</td>
  <td>0.000333</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000333</td>
</tr>
<tr>
  <td align="right">10000</td>
  <td>0.001333</td>
  <td>0.001000</td>
  <td>0.001333</td>
  <td>0.001000</td>
  <td>0.002667</td>
  <td>0.002333</td>
</tr>
<tr>
  <td align="right">100000</td>
  <td>0.015000</td>
  <td>0.014667</td>
  <td>0.014667</td>
  <td>0.014667</td>
  <td>0.021667</td>
  <td>0.018667</td>
</tr>
<tr>
  <td align="right">1000000</td>
  <td>0.253000</td>
  <td>0.255000</td>
  <td>0.250667</td>
  <td>0.250667</td>
  <td>0.266667</td>
  <td>0.262667</td>
</tr>
<tr>
  <td align="right">10000000</td>
  <td>3.905000</td>
  <td>3.821333</td>
  <td>3.638000</td>
  <td>3.642333</td>
  <td>4.016667</td>
  <td>3.987000</td>
</tr>
</table>



<center><img src="http://janm.si/other/quicksort/random.png" alt="Sorting ArrayList of random objects." /></center>


<br />
<p><b>Sorting almost sorted <code>ArrayList</code> of <code>Monkey</code> objects.</b></p>
<p>*Inappropriate pivot picking method results in a worst-case time complexity and the results are immeasurable. More: <a href="https://en.wikipedia.org/wiki/Quicksort#Worst-case_analysis" target="_blank">https://en.wikipedia.org/wiki/Quicksort#Worst-case_analysis</a></p>
<table>
<tr>
    <th>no. of elements</th>
    <th>qs FIRST_ELEMENT</th>
    <th>qs LAST_ELEMENT</th>
    <th>qs MEDIAN</th>
    <th>qs MEDIAN_OF_THREE</th>
    <th>Collections.sort()</th>
    <th>ArrayList.sort()</th>
</tr><tr>
  <td align="right">10</td>
  <td>0.000000</td>
  <td>0.000333</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000000</td>
</tr>
<tr>
  <td align="right">100</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000333</td>
  <td>0.000333</td>
  <td>0.000333</td>
  <td>0.000000</td>
</tr>
<tr>
  <td align="right">1000</td>
  <td>0.000000</td>
  <td>0.000333</td>
  <td>0.000000</td>
  <td>0.000000</td>
  <td>0.000333</td>
  <td>0.000333</td>
</tr>
<tr>
  <td align="right">10000</td>
  <td>0.004667</td>
  <td>0.004333</td>
  <td>0.001000</td>
  <td>0.000667</td>
  <td>0.000667</td>
  <td>0.000667</td>
</tr>
<tr>
  <td align="right">100000</td>
  <td>*</td>
  <td>*</td>
  <td>0.007667</td>
  <td>0.007667</td>
  <td>0.007333</td>
  <td>0.006667</td>
</tr>
<tr>
  <td align="right">1000000</td>
  <td>*</td>
  <td>*</td>
  <td>0.091000</td>
  <td>0.112000</td>
  <td>0.045333</td>
  <td>0.046333</td>
</tr>
<tr>
  <td align="right">10000000</td>
  <td>*</td>
  <td>*</td>
  <td>1.059333</td>
  <td>2.208333</td>
  <td>0.469333</td>
  <td>0.462667</td>
</tr>
</table>

<center><img src="http://janm.si/other/quicksort/almost_sorted.png" alt="Sorting ArrayList of nearly sorted objects." /></center>
