import java.util.ArrayList;
import java.util.Random;

public class Main {

    /**
     * QuickSort example
     */
    public static void main(String[] args) {

        int N = 1000;
        Random random = new Random();
        ArrayList<Monkey> tmp = null;

        //prepare random data
        ArrayList<Monkey> monkeys = new ArrayList<>();
        Monkey[] monkeysArray = new Monkey[N];
        for(int i = 0; i < N; i++){
            monkeys.add(new Monkey("monkey "+i, random.nextInt()));
            monkeysArray[i] = new Monkey("monkey"+i, random.nextInt());
        }

        //QuickSort List, choose median as pivot
        tmp = new ArrayList<>(monkeys);
        QuickSort.sortList(tmp, new QuickSort.Comparator<Monkey>() {
            @Override
            public int compare(Monkey a, Monkey b) {
                if(a.bananaCapacity == b.bananaCapacity) return 0;
                else if (a.bananaCapacity < b.bananaCapacity) return -1;
                else return 1;
            }
        });
        if(isListSorted(tmp)) System.out.println("List is sorted.");
        else System.out.println("List is not sorted.");

        //QuickSort List, choose median of three as pivot (available: MEDIAN, MEDIAN_OF_THREE, FIST_ELEMENT, LAST_ELEMENT)
        tmp = new ArrayList<>(monkeys);
        QuickSort.sortList(tmp, QuickSort.PivotMethod.MEDIAN_OF_THREE, new QuickSort.Comparator<Monkey>() {
            @Override
            public int compare(Monkey a, Monkey b) {
                if(a.bananaCapacity == b.bananaCapacity) return 0;
                else if (a.bananaCapacity < b.bananaCapacity) return -1;
                else return 1;
            }
        });
        if(isListSorted(tmp)) System.out.println("List is sorted.");
        else System.out.println("List is not sorted.");

        //QuickSort array, choose median as pivot
        QuickSort.sortArray(monkeysArray, QuickSort.PivotMethod.MEDIAN_OF_THREE, new QuickSort.Comparator<Monkey>() {
            @Override
            public int compare(Monkey a, Monkey b) {
                if(a.bananaCapacity == b.bananaCapacity) return 0;
                else if (a.bananaCapacity < b.bananaCapacity) return -1;
                else return 1;
            }
        });
        if(isArraySorted(monkeysArray)) System.out.println("Array is sorted.");
        else System.out.println("Array is not sorted.");

    }


    /**
     * Check if list is sorted
     */
    public static boolean isListSorted(ArrayList<Monkey> monkeys){
        for(int i = 1; i < monkeys.size(); i++){
            if(!(monkeys.get(i-1).bananaCapacity <= monkeys.get(i).bananaCapacity)) return false;
        }

        return true;
    }
    /**
     * Check if array is sorted
     */
    public static boolean isArraySorted(Monkey[] monkeys){
        for(int i = 1; i < monkeys.length; i++){
            if(!(monkeys[i-1].bananaCapacity <= monkeys[i].bananaCapacity)) return false;
        }

        return true;
    }

}
