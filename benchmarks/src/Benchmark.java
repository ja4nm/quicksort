import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by glados on 8.4.2017.
 */
public class Benchmark {

    public enum ArrayType{
        RANDOM, ALMOST_SORTED
    }

    private int[] sizes;

    /**
     * Konstruktor
     */
    public Benchmark(){
        this.sizes = new int[]{ 10, 100, 1000, 10000, 100000, 1000000, 10000000 };
    }


    /**
     * Helper methods
     */
    private ArrayList<Integer> generateRandomArray(int size){
        Random random = new Random();
        ArrayList<Integer> array = new ArrayList<>();
        for(int i = 0; i < size; i++){
            array.add(random.nextInt());
        }
        return array;
    }
    private ArrayList<Integer> generateAlmostSortedArray(int size){
        ArrayList<Integer> array = new ArrayList<>();
        Random random = new Random();
        int d = 6;
        if(size <= 10) d = 3;

        array.add(random.nextInt());
        for(int i = 1; i < size; i++){
            int x = (int) (array.get(i-1) + ((random.nextDouble()*2-1) * d));
            array.add(x);
        }

        return array;
    }
    private void createTestCaseFile(ArrayList<Integer> array, File file){
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath())));
            for(int i = 0; i < array.size(); i++){
                writer.write(array.get(i)+"\n");
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private ArrayList<Integer> readTestCaseFile(File file){
        ArrayList<Integer> array = new ArrayList<>();


        try {
            FileInputStream is = new FileInputStream(file.getAbsolutePath());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null)   {
                array.add(Integer.parseInt(line));
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return array;
    }
    private ArrayList<Monkey> intToMonkeyArray(ArrayList<Integer> array){
        ArrayList<Monkey> monkeys = new ArrayList<>();

        for(Integer i : array){
            monkeys.add(new Monkey("monkey "+i, i));
        }

        return monkeys;
    }


    /**
     * Comparators
     */
    private class QuickSortComparator extends QuickSort.Comparator<Monkey> {
        @Override
        public int compare(Monkey a, Monkey b) {
            if(a.bananaCapacity == b.bananaCapacity) return 0;
            else if (a.bananaCapacity < b.bananaCapacity) return -1;
            else return 1;
        }
    }
    private class CollectionComparator implements Comparator<Monkey> {
        @Override
        public int compare(Monkey a, Monkey b) {
            if(a.bananaCapacity == b.bananaCapacity) return 0;
            else if (a.bananaCapacity < b.bananaCapacity) return -1;
            else return 1;
        }
    }


    /**
     * Run benchmark
     */
    public void run(ArrayType arrayType){

        DecimalFormat df = new DecimalFormat("#.######");
        Clock clock = new Clock();
        ArrayList<Monkey> array = null;
        ArrayList<Monkey> monkeys = null;

        // **************************************** PREPARE **********************************
        System.out.println("prepearing benchmark...");
        array = intToMonkeyArray(readTestCaseFile(new File("tests/random_1000000_2.txt")));
        for(int i = 0; i < 15; i++){
            monkeys = new ArrayList<>(array);
            QuickSort.sortList(monkeys, new QuickSortComparator());
            monkeys = null;
            System.gc();
        }
        System.out.println("benchmark started...");
        System.out.println();
        // **************************************** /PREPARE **********************************


        //header
        if(arrayType == ArrayType.RANDOM){
            System.out.println("Random array");
        }else {
            System.out.println("Almost sorted array");
        }
        System.out.println("FIRST_ELEMENT, LAST_ELEMENT, MEDIAN, MEDIAN_OF_THREE, Collections.sort(), ArrayList.sort()");
        System.out.println();


        //for each size run benchmark
        for(int size : sizes){
            double avg = 0;

            //get data
            ArrayList<Monkey>[] arrays = new ArrayList[3];
            for(int j = 0; j < 2; j++){
                if(arrayType == ArrayType.RANDOM){
                    arrays[j] = intToMonkeyArray(readTestCaseFile(new File("tests/random_"+size+"_"+(j+1)+".txt")));
                }else {
                    arrays[j] = intToMonkeyArray(readTestCaseFile(new File("tests/almost_sorted_"+size+"_"+(j+1)+".txt")));
                }
            }

            //quick sort first element
            if(size <= 10000 || arrayType != ArrayType.ALMOST_SORTED){
                avg = 0;
                for(int j = 0; j < 2; j++){
                    monkeys = new ArrayList<>(arrays[j]);
                    clock.start();
                    QuickSort.sortList(monkeys, QuickSort.PivotMethod.FIRST_ELEMENT, new QuickSortComparator());
                    clock.stop();
                    avg += clock.getElapsedSeconds();
                    monkeys = null;
                    System.gc();
                }
                avg = avg / 3.0;
                System.out.print(String.format("%.6f", avg)+";");
            }else {
                System.out.print("-"+";");
            }

            //quick sort last element
            if(size <= 10000 || arrayType != ArrayType.ALMOST_SORTED){
                avg = 0;
                for(int j = 0; j < 2; j++){
                    monkeys = new ArrayList<>(arrays[j]);
                    clock.start();
                    QuickSort.sortList(monkeys, QuickSort.PivotMethod.LAST_ELEMENT, new QuickSortComparator());
                    clock.stop();
                    avg += clock.getElapsedSeconds();
                    monkeys = null;
                    System.gc();
                }
                avg = avg / 3.0;
                System.out.print(String.format("%.6f", avg)+";");
            }else {
                System.out.print("-"+";");
            }

            //quick sort median
            avg = 0;
            for(int j = 0; j < 2; j++){
                monkeys = new ArrayList<>(arrays[j]);
                clock.start();
                QuickSort.sortList(monkeys, QuickSort.PivotMethod.MEDIAN, new QuickSortComparator());
                clock.stop();
                avg += clock.getElapsedSeconds();
                monkeys = null;
                System.gc();
            }
            avg = avg / 3.0;
            System.out.print(String.format("%.6f", avg)+";");

            //quick sort median of three
            avg = 0;
            for(int j = 0; j < 2; j++){
                monkeys = new ArrayList<>(arrays[j]);
                clock.start();
                QuickSort.sortList(monkeys, QuickSort.PivotMethod.MEDIAN_OF_THREE, new QuickSortComparator());
                clock.stop();
                avg += clock.getElapsedSeconds();
                monkeys = null;
                System.gc();
            }
            avg = avg / 3.0;
            System.out.print(String.format("%.6f", avg)+";");

            //java.util.Collections.sort()
            avg = 0;
            for(int j = 0; j < 2; j++){
                monkeys = new ArrayList<>(arrays[j]);
                clock.start();
                Collections.sort(monkeys, new CollectionComparator());
                clock.stop();
                avg += clock.getElapsedSeconds();
                monkeys = null;
                System.gc();
            }
            avg = avg / 3.0;
            System.out.print(String.format("%.6f", avg)+";");

            //java.util.ArrayList.sort()
            avg = 0;
            for(int j = 0; j < 2; j++){
                monkeys = new ArrayList<>(arrays[j]);
                clock.start();
                monkeys.sort(new CollectionComparator());
                clock.stop();
                avg += clock.getElapsedSeconds();
                monkeys = null;
                System.gc();
            }
            avg = avg / 3.0;
            System.out.print(String.format("%.6f", avg)+"");

            System.out.println();
        }

        System.out.println("");
    }

}
