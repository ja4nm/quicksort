/**
 * Created by glados on 18.3.2017.
 */
public class Monkey {

    public String name;
    public int bananaCapacity;

    public Monkey(){}
    public Monkey(String name, int bananaCapacity){
        this.name = name;
        this.bananaCapacity = bananaCapacity;
    }

    public String toString(){
        return this.name+": "+this.bananaCapacity;
    }

}
