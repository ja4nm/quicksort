/**
 * Created by janm on 13. 01. 16.
 */
public class Clock {

    private long start;
    private long end;

    public Clock(){
        reset();
    }

    /**
     * reset clock
     */
    public void reset(){
        start = 0;
        end = 0;
    }

    /**
     * start clock
     */
    public void start(){
        reset();
        start = System.currentTimeMillis();
    }

    /**
     * stop clock
     */
    public void stop(){
        end = System.currentTimeMillis();
    }

    /**
     * get elapsed time
     */
    public long getElapsed(){
        return end - start;
    }
    /**
     * get elapsed time in seconds
     */
    public double getElapsedSeconds(){
        return getElapsed()*0.001;
    }

}
