import java.util.List;

/**
 * Generic QuickSort implementation for sorting arrays (Object[]) and lists (List<T>) in java.
 *
 * by Jan M
 * march 2017
 * http://janm.si
 */
public class QuickSort {

    public enum PivotMethod{
        FIRST_ELEMENT,
        LAST_ELEMENT,
        MEDIAN,
        MEDIAN_OF_THREE
    }

    private HelperMethods helper;
    private Comparator comparator;

    private List list;
    private Object[] array;
    private PivotPickMethod pivotMethod;


    /**
     * Constructor
     */
    public QuickSort(){
        this.pivotMethod = new PivotMedianOfThree();
    }


    /**
     * QuickSort logic
     */
    private void quickSort(int low, int high) {
        int i = low;
        int j = high;

        Object pivot = this.pivotMethod.getPivot(low, high);

        //divide
        while (i <= j) {
            while (this.comparator.compare(this.helper.get(i), pivot) < 0 && i < high) {
                i++;
            }
            while (this.comparator.compare(this.helper.get(j), pivot) > 0 && j > low) {
                j--;
            }

            if (i <= j) {
                this.helper.swap(i, j);
                i++;
                j--;
            }
        }

        //recursion
        if (low < j)
            quickSort(low, j);
        if (i < high)
            quickSort(i, high);
    }


    /**
     * Sort array or list
     */
    public <T> void sort(List<T> list, Comparator<T> comparator){
        this.helper = new ListHelperMethods();
        this.comparator = comparator;
        this.list = list;
        if(this.list.size() == 0) return;
        this.quickSort(0, this.list.size()-1);
    }
    public <T> void sort(T[] array, Comparator<T> comparator){
        this.helper = new ArrayHelperMethods();
        this.comparator = comparator;
        this.array = array;
        if(this.array.length == 0) return;
        this.quickSort(0, this.array.length-1);
    }

    /**
     * Static sort methods
     */
    public static <T> void sortList(List<T> list, Comparator<T> comparator){
        sortList(list, PivotMethod.MEDIAN_OF_THREE, comparator);
    }
    public static <T> void sortList(List<T> list, PivotMethod pivotMethod, Comparator<T> comparator){
        QuickSort quickSort = new QuickSort();
        quickSort.setPivotMethod(pivotMethod);
        quickSort.sort(list, comparator);
    }
    public static <T> void sortArray(T[] array, Comparator<T> comparator){
        sortArray(array, PivotMethod.MEDIAN_OF_THREE, comparator);
    }
    public static <T> void sortArray(T[] array, PivotMethod pivotMethod, Comparator<T> comparator){
        QuickSort quickSort = new QuickSort();
        quickSort.setPivotMethod(pivotMethod);
        quickSort.sort(array, comparator);
    }


    /**
     * Setters
     */
    public void setPivotMethod(PivotMethod method){
        switch (method){
            case FIRST_ELEMENT:
                this.pivotMethod = new PivotFirst();
                break;
            case LAST_ELEMENT:
                this.pivotMethod = new PivotLast();
                break;
            case MEDIAN:
                this.pivotMethod = new PivotMedian();
                break;
            case MEDIAN_OF_THREE:
                this.pivotMethod = new PivotMedianOfThree();
                break;
        }
    }


    /**
     * Comparator
     */
    public static abstract class Comparator<T1>{
        public abstract int compare(T1 a, T1 b);
    }


    /**
     * Helper methods
     */
    private static abstract class HelperMethods{
        protected abstract void swap(int i, int j);
        protected abstract Object get(int i);
    }
    private class ListHelperMethods extends HelperMethods{
        @Override
        protected void swap(int i, int j) {
            Object tmp = list.get(i);
            list.set(i, list.get(j));
            list.set(j, tmp);
        }

        @Override
        protected Object get(int i) {
            return list.get(i);
        }
    }
    private class ArrayHelperMethods extends HelperMethods{
        @Override
        protected void swap(int i, int j) {
            Object tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        @Override
        protected Object get(int i) {
            return array[i];
        }
    }


    /**
     * Pivot methods
     */
    private abstract class PivotPickMethod{
        protected abstract Object getPivot(int low, int high);
    }
    private class PivotFirst extends PivotPickMethod{
        @Override
        protected Object getPivot(int low, int high) {
            return helper.get(low);
        }
    }
    private class PivotLast extends PivotPickMethod{
        @Override
        protected Object getPivot(int low, int high) {
            return helper.get(high);
        }
    }
    private class PivotMedian extends PivotPickMethod{
        @Override
        protected Object getPivot(int low, int high) {
            return helper.get(low + (high-low)/2);
        }
    }
    private class PivotMedianOfThree extends PivotPickMethod{
        @Override
        protected Object getPivot(int low, int high) {
            if(high-low <= 3) helper.get(low + (high-low)/2);

            Object pivotA = helper.get(low);
            Object pivotB = helper.get(low + (high-low)/2);
            Object pivotC = helper.get(high);

            //median of 3
            if(comparator.compare(pivotA, pivotB) < 0){
                if(comparator.compare(pivotA, pivotC) < 0){
                    if(comparator.compare(pivotB, pivotC) < 0){
                        //a,b,c
                        return pivotB;
                    }else {
                        //a,c,b
                        return pivotC;
                    }
                }else {
                    //c,a,b
                    return pivotA;
                }
            }else {
                if(comparator.compare(pivotA, pivotC) < 0){
                    //b,a,c
                    return pivotA;
                }else{
                    if (comparator.compare(pivotB, pivotC) < 0){
                        //b,c,a
                        return pivotC;
                    }else {
                        //c,a,b
                        return pivotA;
                    }
                }
            }
        }
    }

}
