public class Main {


    public static void main(String[] args) {


        Benchmark benchmark = new Benchmark();
        benchmark.run(Benchmark.ArrayType.RANDOM);
        benchmark.run(Benchmark.ArrayType.ALMOST_SORTED);

    }
}
